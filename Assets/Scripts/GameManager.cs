﻿using System.Collections;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour {
    
    #region Vars

    public GameObject playerPrefab;
    public Transform startPoint;
    public int timeToFinish = 30;
    public Transform endPoint;
    public Canvas ui;
    public GameObject pause;
    public GameObject gameOver;
    public TextMeshProUGUI textMesh;
    public AudioSource audioSource;

    private static GameManager instance;
    private Player player;
    private bool paused;
    private int timer;
    private bool isGameOver;
    private IEnumerator timerCoroutine;
    private AudioClip mainClip;
    
    #endregion 
    
    #region Public Methods

    public static GameManager GetInstance() {
        return instance;
    }

    public void SpawnPlayerInStart() {
        player.transform.position = startPoint.position;
        timer = timeToFinish;
        textMesh.text = timer.ToString();
    }

    public bool IsGamePaused() {
        return paused;
    }

    private void TooglePause() {
        paused = !paused;
        pause.SetActive(paused);
        player.ToogleGravity();
        if (paused) {
            StopCoroutine(timerCoroutine);
        }
        else {
            StartCoroutine(timerCoroutine);
        }
    }
    
    #endregion 
    
    #region Private Methods
    
    
    #endregion 

    #region Unity Methods
    
    private void Awake() {
        instance = this;
        if (ui.worldCamera == null) {
            ui.worldCamera = FindObjectOfType<Camera>();
            pause.SetActive(false);
        }
    }
    
    private void Start() {
        player = Instantiate(playerPrefab).GetComponent<Player>();
        pause.SetActive(false);
        SpawnPlayerInStart();
        mainClip = audioSource.clip;
        timer = timeToFinish;
        timerCoroutine = TimerCoroutine();
        StartCoroutine(timerCoroutine);
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape) && !isGameOver) {
            TooglePause();
        }
    }
    
    #endregion 
    
    #region Private Methods

    private IEnumerator TimerCoroutine() {
        while (timer > 0) {
            textMesh.text = timer.ToString();
            yield return new WaitForSeconds(1);
            --timer;
        }
        textMesh.text = timer.ToString();
        ResetPosition();
    }

    private void ResetPosition() {
        player.Death();
        StopCoroutine(timerCoroutine);
        timerCoroutine = TimerCoroutine();
        StartCoroutine(timerCoroutine);
    }

    private void GameOver() {
        paused = true;
        player.ToogleGravity();
        isGameOver = true;
        gameOver.SetActive(true);
    }
    
    #endregion 
}