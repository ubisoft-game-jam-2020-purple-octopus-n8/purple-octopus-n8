﻿using UnityEngine;

public interface ICollisionBehaviour {
    
    void CollisionBehaviour(Player player);
}