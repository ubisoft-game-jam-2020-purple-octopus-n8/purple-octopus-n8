﻿using UnityEngine;

public class End : MonoBehaviour, ICollisionBehaviour {
    
    #region Vars

    public string nextScene;
    public AudioSource audioSource;
    public AudioClip clip;
    
    #endregion
    
    #region Collision Methods

    public void CollisionBehaviour(Player player) {
        SceneLoader.LoadScene(nextScene);
        audioSource.PlayOneShot(clip);
    }
    
    #endregion
    
    #region Unity Methods

    private void Awake() {
        if (audioSource == null) {
            audioSource = GetComponent<AudioSource>();
        }
    }
    
    #endregion
}