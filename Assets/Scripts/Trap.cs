﻿using UnityEngine;

public class Trap : MonoBehaviour, ICollisionBehaviour {
    
    #region Vars

    public AudioSource audioSource;
    public AudioClip clip;
    
    #endregion
    
    #region Collision Methods

    public void CollisionBehaviour(Player player) {
        player.Death(); 
        audioSource.PlayOneShot(clip);
    }
    
    #endregion
    
    #region Unity Methods

    private void Awake() {
        if (audioSource == null) {
            audioSource = GetComponent<AudioSource>();
        }
    }
    
    #endregion
}