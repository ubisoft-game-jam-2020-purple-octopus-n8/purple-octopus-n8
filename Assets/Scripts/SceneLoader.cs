﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
    
    #region Scene Methods

    public static void LoadScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    public static void ExitGame() {
        Application.Quit();
    }

    public static void RestartScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    #endregion
}