﻿using UnityEngine;

public class Player : MonoBehaviour {
    
    #region Vars

    public float velocity = 2f;
    public float gravity = 3f;
    public float movementSmoothing = .05f;
    public Animator animator;
    public AudioSource audioSource;
    public AudioClip changeGravityClip;
    public AudioClip deathClip;

    private Rigidbody2D rigidbody;
    private Vector2 move;
    private Vector2 currentVelocity = Vector2.zero;
    private bool enableGravity = true;
    private float currentGravity;

    #endregion
    
    #region Public Methods

    public void Death() {
        rigidbody.gravityScale = gravity;
        GameManager.GetInstance().SpawnPlayerInStart();
        audioSource.PlayOneShot(deathClip);
        animator.SetBool("Reverse", false);
    }

    public void ToogleGravity() {
        enableGravity = !enableGravity;
        if (enableGravity) {
            rigidbody.gravityScale = currentGravity;
        }
        else {
            currentGravity = rigidbody.gravityScale;
            rigidbody.gravityScale = 0;
            rigidbody.velocity = Vector2.zero;
        }
    }
    
    #endregion

    #region Unity Methods

    private void Awake() {
        rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.gravityScale = gravity;
        //audioSource.clip = changeGravityClip;
    }

    private void Update() {
        PerformActions();
    }

    private void FixedUpdate() {
        PerformMovement();
    }

    private void OnTriggerEnter2D(Collider2D col) {
        Debug.Log($"Collider: {col.name}");
        ICollisionBehaviour collisionBehaviour = col.GetComponentInParent<ICollisionBehaviour>();
        collisionBehaviour?.CollisionBehaviour(this);
    }

    #endregion

    #region Movement Methods

    private void PerformMovement() {
        if (!GameManager.GetInstance().IsGamePaused()) {
            rigidbody.velocity = Vector2.SmoothDamp(rigidbody.velocity, move, ref currentVelocity, movementSmoothing);
        }
    }

    private void PerformActions() {
        if (!GameManager.GetInstance().IsGamePaused()) {
            move.x = Input.GetAxis("Horizontal") * velocity;
            animator.SetFloat("HorizontalSpeed", move.x);
            if (Input.GetKeyDown(KeyCode.Space)) {
                rigidbody.gravityScale *= -1;
                animator.SetBool("Reverse", rigidbody.gravityScale > 0 ? false : true);
                audioSource.PlayOneShot(changeGravityClip);
            }
        }
    }

    #endregion
}