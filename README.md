# Go Fishy Go Goldie

![](https://img.itch.zone/aW1nLzQ2OTA4OTAuZ2lm/original/18F4zc.gif)

The NASA has captured you in your contaminated port and sent you to Mars as a guinea pig to see if you survive. You find yourself in a desert without water (Mars), only the water you have in your suit.

You have a short amount of time to reach the end and manage to survive without drying up.

Get to the end of each level and help him return home! 

## Download

You can download the game on its Itchio page: https://irenete.itch.io/go-goldie

## Controls

Move -> AD \
Invert Gravity -> Space

## Developers

This game was developed by:

Game / Level Design:

**Roque Bru Navarro** -> [@RBN_Games](https://twitter.com/RBN_Games) / [Portfolio](https://www.rbngames.online/)  / RockBRNavarro@gmail.com \
**Irene Marina Barbé** -> [@IreneMarinaB](https://twitter.com/IreneMarinaB) / [Portfolio](https://www.thebirdfree.com/en/irenemarinab/) / monalpes@gmail.com

Programming:

**Javier Osuna Herrera** -> [@javosuher](https://twitter.com/javosuher) / [LinkedIn](https://www.linkedin.com/in/javierosunaherrera/) / javier.osunaherrera@gmail.com

Art:

**Guillermo Marsilla Alandi** -> [gmarsilla_art](https://www.instagram.com/gmarsilla_art/) / [@GMarsilla](https://twitter.com/GMarsilla) / [Portfolio](http://gmarsilla.com/) /  guillermomarsilla@gmail.com

Music / Sounds: 

**Anthony Fiori** -> [@anthonyfiori8](https://twitter.com/anthonyfiori8) / [anthonyfiori8](https://www.instagram.com/anthonyfiori8/) / [Portfolio](https://www.behance.net/anthonyfiori) / pablofloresalosi@gmail.com
